# NLP-Twitter-Sentiment-Analysis

### NLP: Twitter Sentiment Analysis

Now we will perform Twitter sentiment analysis. We will accomplish this by completing each task in the project:

- Task #1: Understand the Problem Statement and business case
- Task #2: Import libraries and datasets
- Task #3: Perform Exploratory Data Analysis
- Task #4: Plot the word cloud
- Task #5: Perform data cleaning - removing punctuation
- Task #6: Perform data cleaning - remove stop words
- Task #7: Perform Count Vectorization (Tokenization)
- Task #8: Create a pipeline to remove stop-words, punctuation, and perform tokenization
- Task #9: Understand the theory and intuition behind Naive Bayes classifiers
- Task #10: Train a Naive Bayes Classifier
- Task #11: Assess trained model performance


# Sentimental Analysis on COVID-19 Tweets using python

### Course Objectives

In this course, we are going to focus on 3 learning objectives:

- Learn how to Preprocess text data for Sentimental Analysis  

- Learn how to Label text data with positive, negative and neutral sentiments 

- Learn to visualize the result of sentiment Analysis 

By the end of this project you will learn how to preprocess your text data for sentimental analysis. So in this project we are going to use a Dataset consisting of data related to the tweets from 24th of july,2020 to 30th of august 2020 with covid19 hashtags.  we are going to use python to apply sentimental analysis on the tweets to see people reactions to the pandemic during the mentioned period. we are going to label the tweets as Positive ,Negative and neutral. after that we are going to visualize the result to see the people reactions on twitter. 

### Project Structure

The hands on project on Sentimental Analysis on COVID-19 Tweets using pythonis divided into following tasks:

- Task 1: Introduction and Loading our Dataset    
in this task we will talk about the purpose of this course and the things that you are going  to learn at the end of this project. We will import our csv file.     

- Task 2: Text preprocessing     
By the end of this task you will learn how to preprocess and prepare our text data for Sentimental Analysis 

- Task 3: Let’s Explore our Date    
 In this task we will visualize most common words using a bar chart.

- Task 4:  Sentimental Analysis    
In this task we will use NLTK module to produce Polarity scores for each tweet  

- Task 5:  Visualizing the Result    
 In this task we will visualize the result of our analysis using line chart   

https://raw.githubusercontent.com/gabrielpreda/covid-19-tweets/master/covid19_tweets.csv
